# -*- coding: utf-8 -*-
'''
Задание 1.5

Из строк pages1 и pages2 получить список страниц,
которые есть и в pages1  и в  pages2.

Результатом должен быть список: ['1', '3', '8']

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

pages1 = 'интересные страницы: 1,2,3,5,8'
pages2 = 'интересные страницы: 1,3,8,9'

list1 = pages1[21:].split(',')
list2 = pages2[21:].split(',')

print (list(set(list1).intersection(list2))) ## prints UNSORTED list

list3 = list(set(list1).intersection(list2))
list3.sort()
print(list3)

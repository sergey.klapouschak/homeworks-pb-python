# -*- coding: utf-8 -*-
'''
Задание 1.6

Обработать строку vova и вывести информацию на стандартный поток вывода в виде:
name:                  Владимир
ip:                    10.0.13.3
город:                 Moscow
date:                  15.03.2020
time:                  15:20

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

vova = 'O Владимир       15.03.2020 15:20 10.0.13.3, 3d18h, Moscow/5'

vova_array=vova.replace(',', ' ').replace('/', ' ').split()

print('name: \t\t' + vova_array[1])
print('ip: \t\t' + vova_array[4])
print('город: \t\t' + vova_array[6])
print('date: \t\t' + vova_array[2])
print('time: \t\t' + vova_array[3])

